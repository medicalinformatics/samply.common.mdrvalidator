/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.dth.mdr.validator.formats;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import de.dth.mdr.validator.enums.EnumDateFormat;

public class DateFormats {
    /**
     * Get the date format pattern from the MDR date format description that the date/time picker can understand.
     * 
     * @param enumDateFormat
     *            the date format as known in MDR
     * @return the date format pattern string representation that the date/time picker can understand
     */
    public static String getDatepickerPattern(final EnumDateFormat enumDateFormat) {
        String datePattern = getDatePattern(enumDateFormat);
        return datePattern.toUpperCase();
    }

    /**
     * Get the date format pattern from the MDR date format description.
     * 
     * @param enumDateFormat
     *            the date format as known in MDR
     * @return the date format pattern string representation
     */
    public static String getDatePattern(final EnumDateFormat enumDateFormat) {
        DateFormat formatter;
        switch (enumDateFormat) {
        case DIN_5008:
            formatter = new SimpleDateFormat("MM.yyyy");
            break;
        case DIN_5008_WITH_DAYS:
            formatter = new SimpleDateFormat("dd.MM.yyyy");
            break;
        case ISO_8601:
            formatter = new SimpleDateFormat("yyyy-MM");
            break;
        case ISO_8601_WITH_DAYS:
            formatter = new SimpleDateFormat("yyyy-MM-dd");
            break;
        case LOCAL_DATE: // not valid
        case LOCAL_DATE_WITH_DAYS:
        default:
            formatter = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault());
            break;
        }
        return ((SimpleDateFormat) formatter).toPattern();
    }
}
