/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.dth.mdr.validator.formats;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import de.dth.mdr.validator.enums.EnumTimeFormat;

public class TimeFormats {
    /**
     * Get the date format pattern from the MDR date format description that the timepicker can understand.
     * 
     * @param enumTimeFormat
     *            the date format as known in MDR
     * @return the date format pattern string representation that the timepicker understands
     */
    public static String getTimepickerPattern(final EnumTimeFormat enumTimeFormat) {
        String datePattern = getTimePattern(enumTimeFormat);
        return datePattern.toUpperCase();
    }

    /**
     * Get the date format pattern from the MDR date format description.
     * 
     * @param enumTimeFormat
     *            the date format as known in MDR
     * @return the date format pattern string representation
     */
    public static String getTimePattern(final EnumTimeFormat enumTimeFormat) {
        DateFormat formatter;
        switch (enumTimeFormat) {
        case HOURS_24:
            formatter = new SimpleDateFormat("HH:mm");
            break;
        case HOURS_24_WITH_SECONDS:
            formatter = new SimpleDateFormat("HH:mm:ss");
            break;
        case HOURS_12:
            formatter = new SimpleDateFormat("h:mm a");
            break;
        case HOURS_12_WITH_SECONDS:
            formatter = new SimpleDateFormat("h:mm:ss a");
            break;
        case LOCAL_TIME:
            formatter = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault());
            break;
        case LOCAL_TIME_WITH_SECONDS:
            formatter = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.getDefault());
            break;
        default:
            formatter = new SimpleDateFormat("HH:mm");
            break;
        }
        return ((SimpleDateFormat) formatter).toPattern();
    }
}
