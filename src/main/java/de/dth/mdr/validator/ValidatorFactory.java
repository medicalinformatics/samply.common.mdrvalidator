/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.dth.mdr.validator;

import de.dth.mdr.validator.enums.EnumValidatorType;
import de.dth.mdr.validator.validators.*;

public class ValidatorFactory {
    /**
     * Factory to give a Validator based on type.
     *
     * @param type  validator type
     * @param unitOfMeasure the unit of measure if there is one
     * @param flatCatalogue the flattened catalogue (only for catalogue validator)
     * @return
     */
    public static Validator getValidator(EnumValidatorType type, String unitOfMeasure, FlatCatalogue flatCatalogue) {
        Validator validator = null;

        switch (type) {
        case BOOLEAN:
            validator = new BooleanValidator();
            break;
        case INTEGER:
            validator = new IntegerValidator();
            break;
        case INTEGERRANGE:
            validator = new IntegerRangeValidator();
            break;
        case FLOAT:
            validator = new FloatValidator();
            break;
        case FLOATRANGE:
            validator = new FloatRangeValidator();
            break;
        case DATE:
            validator = new DateValidator();
            break;
        case TIME:
            validator = new TimeValidator();
            break;
        case DATETIME:
            validator = new DateTimeValidator();
            break;
        case REGEX:
            validator = new MdrRegexValidator();
            break;
        case CATALOG:
            validator = new CatalogueValidator();
            break;
        case ENUMERATED:
            validator = new PermissibleValueValidator();
            break;
        case STRING:
            validator = new StringValidator();
            break;
        case NONE:
            validator = null;
            break;
        default:
            break;
        }

        if(validator != null) {
            if(unitOfMeasure != null)
                validator.setUnitOfMeasure(unitOfMeasure);

            if(validator instanceof CatalogueValidator && flatCatalogue != null) {
                ((CatalogueValidator) validator).setFlatCatalogue(flatCatalogue);
            }
        }

        return validator;
    }

    /**
     * Factory to give a Validator based on type.
     *
     * @param type  validator type
     * @return
     */
    public static Validator getValidator(EnumValidatorType type) {
        return getValidator(type, null, null);
    }
}
