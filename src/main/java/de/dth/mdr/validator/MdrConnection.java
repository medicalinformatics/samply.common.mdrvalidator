/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.dth.mdr.validator;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import de.samply.auth.client.jwt.JWTException;
import de.samply.auth.client.jwt.KeyLoader;
import de.samply.auth.rest.AccessTokenDTO;
import de.samply.auth.rest.AccessTokenRequestDTO;
import de.samply.auth.rest.KeyIdentificationDTO;
import de.samply.auth.rest.SignRequestDTO;
import de.samply.common.http.HttpConnector;
import de.samply.common.http.HttpConnectorException;
import de.samply.common.mdrclient.MdrClient;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.configuration.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URISyntaxException;
import java.security.*;
import java.util.*;

/**
 * Provides a Connector to the MDR
 */
public class MdrConnection {
    private final Logger logger = LoggerFactory.getLogger(MdrConnection.class);
    
    private MdrClient mdrClient;
    private AccessTokenDTO accessToken;
    private Configuration configuration;

    private String authUserId;
    private String keyId;
    private String authURL;
    private String privateKeyBase64;
    private List<String> namespaces;
    private HttpConnector httpConnector;


    /**
     * Establishes connection to the MDR
     *
     * @param mdrURL    URL to MDR REST
     * @param authUserId    The userID to log in with
     * @param keyId     The keyId to use
     * @param authURL   URL to AUTH REST
     * @param privateKeyBase64  The private key (base64 encoded)
     * @param namespace The MDR namespace to use
     * @param anonymous Access MDR anonymous way
     * @param httpConnector The httpConnector43 to use
     * @throws URISyntaxException
     */
    public MdrConnection(String mdrURL, String authUserId, String keyId, String authURL, String privateKeyBase64, String namespace, boolean anonymous, HttpConnector httpConnector) throws URISyntaxException {
        initialize (mdrURL, authUserId, keyId, authURL, privateKeyBase64, new ArrayList<>(Collections.singletonList(namespace)), null, anonymous, httpConnector);
    }

    /**
     * Establishes connection to the MDR
     *
     * @param mdrURL    URL to MDR REST
     * @param authUserId    The userID to log in with
     * @param keyId     The keyId to use
     * @param authURL   URL to AUTH REST
     * @param privateKeyBase64  The private key (base64 encoded)
     * @param namespaces The MDR namespaces to use
     * @param anonymous Access MDR anonymous way
     * @param httpConnector The httpConnector43 to use
     * @throws URISyntaxException
     */
    public MdrConnection(String mdrURL, String authUserId, String keyId, String authURL, String privateKeyBase64, List<String> namespaces, boolean anonymous, HttpConnector httpConnector) throws URISyntaxException {
        initialize (mdrURL, authUserId, keyId, authURL, privateKeyBase64, namespaces, null, anonymous, httpConnector);
    }

    /**
     * Establishes connection to the MDR
     * 
     * @param mdrURL    URL to MDR REST
     * @param authUserId    The userID to log in with
     * @param keyId     The keyId to use
     * @param authURL   URL to AUTH REST
     * @param privateKeyBase64  The private key (base64 encoded)
     * @param namespace The MDR namespace to use
     * @param proxy Proxy configuration (as de.samply.common.config.Configuration)
     * @param anonymous Access MDR anonymous way
     * @throws URISyntaxException 
     */
    public MdrConnection(String mdrURL, String authUserId, String keyId, String authURL, String privateKeyBase64, String namespace, de.samply.common.config.Configuration proxy, boolean anonymous) throws URISyntaxException {
        initialize (mdrURL, authUserId, keyId, authURL, privateKeyBase64, new ArrayList<>(Collections.singletonList(namespace)), proxy, anonymous, null);

    }

    /**
     * Establishes connection to the MDR
     *
     * @param mdrURL    URL to MDR REST
     * @param authUserId    The userID to log in with
     * @param keyId     The keyId to use
     * @param authURL   URL to AUTH REST
     * @param privateKeyBase64  The private key (base64 encoded)
     * @param namespaces The MDR namespaces to use
     * @param proxy Proxy configuration (as de.samply.common.config.Configuration)
     * @param anonymous Access MDR anonymous way
     * @throws URISyntaxException
     */
    public MdrConnection(String mdrURL, String authUserId, String keyId, String authURL, String privateKeyBase64, List<String> namespaces, de.samply.common.config.Configuration proxy, boolean anonymous) throws URISyntaxException {
        initialize (mdrURL, authUserId, keyId, authURL, privateKeyBase64, namespaces, proxy, anonymous, null);

    }

    /**
     * Establishes connection to the MDR
     *
     * @param mdrURL    URL to MDR REST
     * @param authUserId    The userID to log in with
     * @param keyId     The keyId to use
     * @param authURL   URL to AUTH REST
     * @param privateKeyBase64  The private key (base64 encoded)
     * @param namespace The MDR namespace to use
     * @throws URISyntaxException
     */
    public MdrConnection(String mdrURL, String authUserId, String keyId, String authURL, String privateKeyBase64, String namespace) throws URISyntaxException {
        initialize(mdrURL, authUserId, keyId, authURL, privateKeyBase64, new ArrayList<>(Collections.singletonList(namespace)), null, false, null);
    }

    /**
     * Establishes connection to the MDR
     *
     * @param mdrURL    URL to MDR REST
     * @param authUserId    The userID to log in with
     * @param keyId     The keyId to use
     * @param authURL   URL to AUTH REST
     * @param privateKeyBase64  The private key (base64 encoded)
     * @param namespaces The MDR namespaces to use
     */
    public MdrConnection(String mdrURL, String authUserId, String keyId, String authURL, String privateKeyBase64, List<String> namespaces) throws URISyntaxException {
        initialize(mdrURL, authUserId, keyId, authURL, privateKeyBase64, namespaces, null, false, null);
    }

    private void initialize (String mdrURL, String authUserId, String keyId, String authURL, String privateKeyBase64, List<String> namespaces, de.samply.common.config.Configuration proxy, boolean anonymous, HttpConnector httpConnector43) {

        this.authURL = authURL;
        this.authUserId = authUserId;
        this.keyId = keyId;
        this.privateKeyBase64 = privateKeyBase64;
        this.namespaces = namespaces;

        try {
            if (httpConnector43 != null) {
                httpConnector = httpConnector43;
            } else if (proxy != null) {
                httpConnector = new HttpConnector(proxy);
            }else {
                httpConnector = new HttpConnector(new HashMap<String, String>());
            }


            Client client = httpConnector.getClient(httpConnector.getHttpClientForHTTPS());
            mdrClient = new MdrClient(mdrURL, client);

            if(!anonymous)
                accessToken = getAccessToken(client);
        } catch (HttpConnectorException | InvalidKeyException | NoSuchAlgorithmException | SignatureException e) {
            e.printStackTrace();
        }

    }

    /**
     * Closes the connections
     */
    public void close() {
        httpConnector.closeClients();
    }
    
    /**
     * Gets an AccessTokenDTO accessToken from Samply.AUTH component, used for
     * communication with other components
     *
     * @param client
     *            A Jersey client
     * @return AccessTokenDTO
     * @throws NoSuchAlgorithmException
     *             the no such algorithm exception
     * @throws InvalidKeyException
     *             the invalid key exception
     * @throws SignatureException
     *             the signature exception
     */
    public AccessTokenDTO getAccessToken(Client client)
            throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        if (client == null)
            return null;

        KeyIdentificationDTO identification = new KeyIdentificationDTO();

        String keyId = this.keyId;
        if (keyId == null || "".equalsIgnoreCase(keyId))
            return null;

        identification.setKeyId(Integer.parseInt(keyId));

        ClientResponse response = client.resource(authURL + "/oauth2/signRequest").accept("application/json")
                .type("application/json").post(ClientResponse.class, identification);
        if (response.getStatus() != 200) {
            logger.debug("Auth.getAccessToken returned " + response.getStatus() + " on signRequest bailing out!");
            return null;
        }
        SignRequestDTO signRequest = response.getEntity(SignRequestDTO.class);
        
        /**
         * Sign the code and encode to base64
         */
        Signature sig = Signature.getInstance(signRequest.getAlgorithm());
        PrivateKey privkey = KeyLoader.loadPrivateKey(privateKeyBase64);
        sig.initSign(privkey);
        sig.update(signRequest.getCode().getBytes());
        String signature = Base64.encodeBase64String(sig.sign());

        AccessTokenRequestDTO accessRequest = new AccessTokenRequestDTO();
        accessRequest.setCode(signRequest.getCode());
        accessRequest.setSignature(signature);

        response = client.resource(authURL + "/oauth2/access_token").accept("application/json").type("application/json")
                .post(ClientResponse.class, accessRequest);

        if (response.getStatus() != 200) {
            logger.debug("Auth.getAccessToken returned " + response.getStatus() + " bailing out!");
            return null;
        }

        return response.getEntity(AccessTokenDTO.class);
    }

    /**
     * Gets the mdr client
     * @return the used mdr client
     */
    public MdrClient getMdrClient() {
        return mdrClient;
    }

    public String getAccessTokenToken() {
        if(accessToken == null)
            return null;
        
        return accessToken.getAccessToken();
    }

    /**
     * Gets the access token
     * @return the access token
     */
    public AccessTokenDTO getAccessToken() {
        return accessToken;
    }

    /**
     * Gets the configuration
     * @return the configuration
     */
    public Configuration getConfiguration() {
        return configuration;
    }

    /**
     * Gets the AuthUserId
     * @return the AuthUserId
     */
    public String getAuthUserId() {
        return authUserId;
    }

    /**
     * Gets the mdr namespaces
     * @return the list of mdr namespaces
     */
    public List<String> getNamespaces() {
        return namespaces;
    }
}
