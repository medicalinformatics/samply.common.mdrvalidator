/*
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 *
 */
package de.dth.mdr.validator.validators;

import de.dth.mdr.validator.FlatCatalogue;
import de.dth.mdr.validator.exception.ValidatorException;
import de.samply.common.mdrclient.domain.Validations;

/**
 * Validator for a catalogue. It checks if the value is a valid code.
 * If not, it will also check if the value is a valid designation.
 */
public class CatalogueValidator extends Validator {
    private FlatCatalogue flatCatalogue;

    /**
     * Info if or not a value was validated by designation instead of code
     */
    private Boolean wasValidatedDesignation = false;

    @Override
    public final boolean validate(Validations dataElementValidations, final Object value) throws ValidatorException {
        wasValidatedDesignation = false;

        if (value == null || value.toString().equals("")) {
            return true;
        }

        if (isCaseSensitive()) {
            return validateCaseSensitive(value);
        } else{
            return validateIgnoreCase(value);
        }

    }

    private boolean validateIgnoreCase(final Object value) {
        // check code
        for (String validCode : flatCatalogue.getValidCodes()) {
            // case sensitive
            if (validCode.equalsIgnoreCase(value.toString()))
                return true;
        }

        // check designation
        for (String validDesignation : flatCatalogue.getValidDesignationToCodes().keySet()) {
            // case sensitive
            if (validDesignation.equalsIgnoreCase(value.toString()))
                return true;
        }

        return false;
    }

    private boolean validateCaseSensitive(final Object value) {
        // check code
        if (flatCatalogue.getValidCodes().contains(value.toString())) {
            return true;
        }

        // check designation
        if (flatCatalogue.getValidDesignationToCodes().containsKey(value.toString())) {
            wasValidatedDesignation = true;
            return true;
        }

        return false;
    }

    public void setFlatCatalogue(FlatCatalogue flatCatalogue) {
        this.flatCatalogue = flatCatalogue;
    }

    public FlatCatalogue getFlatCatalogue() {
        return flatCatalogue;
    }

    public Boolean getWasValidatedDesignation() {
        return wasValidatedDesignation;
    }
}