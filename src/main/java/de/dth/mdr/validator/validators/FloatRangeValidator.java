/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.dth.mdr.validator.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.dth.mdr.validator.exception.ValidatorException;
import de.samply.common.mdrclient.domain.EnumValidationType;
import de.samply.common.mdrclient.domain.Validations;

/**
 * Validator for float numbers between a maximum and a minimum value.
 */
public class FloatRangeValidator extends Validator {
    /**
     * A float range regular expression, as it is known in the MDR.
     */
    static final String FLOAT_RANGE_REGEX = "(?:(.+)<=)?x(?:<=(.+))?";

    @Override
    public final boolean validate(Validations dataElementValidations, final Object value)
            throws ValidatorException {
        
        if (value == null || value.toString().equals("")) {
            return true;
        }
        
        boolean retValue = false;
        
        try {
            if (dataElementValidations.getValidationType().equals(EnumValidationType.FLOATRANGE.name())) {
                // check if it is a valid range
                Pattern pattern = Pattern.compile(FLOAT_RANGE_REGEX);
                Matcher matcher = pattern.matcher(dataElementValidations.getValidationData());

                if (matcher.find()) {
                    String min = matcher.group(1);
                    String max = matcher.group(2);

                    Float minFloat = null;
                    if(min != null)
                        minFloat = Float.valueOf(min);
                    Float maxFloat = null;
                    if(max != null)
                        maxFloat = Float.valueOf(max);

                    String checkMe = value.toString();
                    if(getUnitOfMeasure() != null) {
                        checkMe = cutUnitOfMeasure(checkMe);
                    }

                    Float floatValue = Float.valueOf(String.valueOf(checkMe));

                    if (minFloat != null && floatValue < minFloat || maxFloat != null && floatValue > maxFloat) {
                        retValue = false;
                    } else {
                        retValue = true;
                    }
                }
            }
        } catch (NumberFormatException e) {
            retValue = false;
        }
        
        return retValue;
    }
}