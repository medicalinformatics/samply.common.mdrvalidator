/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.dth.mdr.validator.validators;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import de.dth.mdr.validator.exception.ValidatorException;
import de.dth.mdr.validator.formats.DateTimeFormats;
import de.samply.common.mdrclient.domain.Validations;

/**
 * Validator for date and time inputs.
 */
public class DateTimeValidator extends Validator {
    @Override
    public boolean validate(Validations dataElementValidations, final Object value) throws ValidatorException {
        // get the enum date and time format from the mdr data element
        DateTimeFormats dateTimeFormats = DateTimeFormats
                .getDateTimeFormats(dataElementValidations.getValidationData());

        if(value == null || value.toString().equals("")){
            return true;
        }
        // translate it to a pattern
        String dateTimePattern = DateTimeFormats.getDateTimePattern(dateTimeFormats.getDateFormat(),
                dateTimeFormats.getTimeFormat());
        SimpleDateFormat sdf = new SimpleDateFormat(dateTimePattern);
        sdf.setLenient(false);

        try {
            // if not valid, it will throw ParseException
            sdf.parse(value.toString());
            return true;
        } catch (ParseException e) {
        }

        return false;
    }

}