/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.dth.mdr.validator.utils;

import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.domain.Result;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class Utils {
    /**
     * Uppers the first char of a string.
     *
     * @param string the string
     * @return the string
     */
    public static String upperCaseFirstChar(String string) {
        if (string == null || string.length() == 0) {
            return string;
        }

        char c[] = string.toCharArray();
        c[0] = Character.toUpperCase(c[0]);
        string = new String(c);

        return string;
    }

    /**
     * Gets all dataelements from a group and its subgroups.
     *
     * @param mdrClient a pre-initialized mdr client instance
     * @param languageCode the two-letter language code. e.g. "en" or "de"
     * @param theList the list to extend with new dataelement ids
     * @param groupKey the mdr id of the dataelementgroup to add
     * @return the elements from the group and its subgroups
     */
    public static List<String> getElementsFromGroupAndSubgroups(MdrClient mdrClient, String languageCode, List<String> theList, String groupKey) {
        try {
            List<Result> result_l = mdrClient.getMembers(groupKey, languageCode);
            for (Result r : result_l) {
                if (r.getType().equalsIgnoreCase("dataelementgroup")) {
                    theList = getElementsFromGroupAndSubgroups(mdrClient, languageCode, theList, r.getId());
                } else {
                    theList.add(r.getId());
                }
            }
        } catch (MdrConnectionException | ExecutionException e) {

        }

        return theList;
    }
}
