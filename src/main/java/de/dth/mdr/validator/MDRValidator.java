/**
 * Copyright (C) 2016 Medizinische Informatik in der Translationalen Onkologie,
 * Deutsches Krebsforschungszentrum in Heidelberg
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see http://www.gnu.org/licenses.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.dth.mdr.validator;

import de.dth.mdr.validator.enums.EnumValidatorType;
import de.dth.mdr.validator.exception.MdrException;
import de.dth.mdr.validator.exception.ValidatorException;
import de.dth.mdr.validator.utils.Utils;
import de.dth.mdr.validator.validators.CatalogueValidator;
import de.dth.mdr.validator.validators.Validator;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.*;
import de.samply.string.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

/**
 * A MDR validator for DTH
 *
 * TODO: Support for limited list of MDRkeys
 */
public class MDRValidator {
    private final Logger logger = LoggerFactory.getLogger(MDRValidator.class);

    /** List of ambiguous catalogues with which we cannot transform designations to codes */
    private static List<String> ambiguousCatalogues;

    /** Info if the last value was validated by catalogue designation instead of code */
    private Boolean lastValidatedWasCatalogueDesignation = false;

    /** Matrix that contains the MDR Validations entries per MDRKey */
    private HashMap<String, Validations> validationMatrix;

    /** Matrix that contains the FlatCatalogue entry per MDRKey */
    private HashMap<String, FlatCatalogue> flatCatalogueMatrix;

    /**
     * Stores a Slot entry -> MdrKey Matrix to quickly find the dataelement
     * belonging to a slot
     */
    private HashMap<String, HashMap<String, MdrKey>> slotMdrKeyMatrix;

    /** The MDR Connection */
    private MdrConnection mdrConnection;

    private final String caseSensitive = "caseSensitive";

    private List<MdrKey> baseKeys = new ArrayList<>();

    public MDRValidator(MdrConnection mdrConnection, String... dataelementgroups)
            throws MdrConnectionException, ExecutionException, MdrException, MdrInvalidResponseException, ValidatorException {
        this(mdrConnection, false, dataelementgroups);

    }

    public MDRValidator(MdrConnection mdrConnection, boolean preLoadDataelements, String... dataelementgroups)
            throws MdrConnectionException, ExecutionException, MdrException, MdrInvalidResponseException, ValidatorException {
        this.mdrConnection = mdrConnection;
        validationMatrix = new HashMap<>();
        flatCatalogueMatrix = new HashMap<>();
        slotMdrKeyMatrix = new HashMap<>();
        List<String> groupAndSubgroupMembers = new ArrayList<>();

        if (dataelementgroups.length == 0) {
            initMembers(preLoadDataelements);
        } else {
            for (String groupKey : dataelementgroups) {
                groupAndSubgroupMembers = Utils.getElementsFromGroupAndSubgroups(mdrConnection.getMdrClient(), "en", groupAndSubgroupMembers, groupKey);
                for (String key : groupAndSubgroupMembers) {
                    MdrKey baseKey = new MdrKey(key, mdrConnection);
                    baseKeys.add(baseKey);
                    logger.info("Reading base key " + key);
                    initMember(baseKey, preLoadDataelements);
                }
            }
        }

    }

    /** The root elements of the validator */
    public List<MdrKey> getBaseKeys() {
        return baseKeys;
    }

    /**
     * Initializes one mdrKey without preloading the dataelement
     *
     * @param mdrKey
     *            the key to initialize
     * @throws ExecutionException
     * @throws MdrConnectionException
     * @throws MdrException
     * @throws MdrInvalidResponseException
     */
    private void initMember(MdrKey mdrKey) throws MdrConnectionException, ExecutionException, MdrInvalidResponseException, MdrException, ValidatorException {
        initMember(mdrKey, false);
    }

    /**
     * Initializes one mdrKey
     *
     * @param mdrKey
     *            the key to initialize
     * @param preLoadDataelement
     *            should the whole dataelement be preloaded instead of loading validations and slots separately
     * @throws ExecutionException
     * @throws MdrConnectionException
     * @throws MdrException
     * @throws MdrInvalidResponseException
     */
    private void initMember(MdrKey mdrKey, boolean preLoadDataelement) throws MdrConnectionException, ExecutionException, MdrInvalidResponseException, MdrException, ValidatorException {
        if (mdrKey.isRecord()) {
            List<Result> members = mdrConnection.getMdrClient().getRecordMembers(mdrKey.toString(),
                    Locale.getDefault().getLanguage(), mdrConnection.getAccessTokenToken(),
                    mdrConnection.getAuthUserId());

            for (Result result : members) {
                //Going recursively through the child keys
                initMember(new MdrKey(result.getId(), mdrConnection), preLoadDataelement);
            }

            //TODO: Disabled for now, because "find mdrkey by slotname" would return the record in most cases
            // We need to clean up the MDR record slot names or build a real functionality to support validation "as record"
            //readSlots(mdrKey);
            return;
        }
        
        if (mdrKey.isGroup()) {
            List<Result> members = mdrConnection.getMdrClient().getMembers(mdrKey.toString(),
                    Locale.getDefault().getLanguage(), mdrConnection.getAccessTokenToken(),
                    mdrConnection.getAuthUserId());
            
            //Going recursively through the child keys
            for (Result result : members) {
                initMember(new MdrKey(result.getId(), mdrConnection), preLoadDataelement);
            }
            readSlots(mdrKey);
            return;
        }

        if (preLoadDataelement) {
            mdrKey.loadDataElement();
        }

        validationMatrix.put(mdrKey.toString(), mdrKey.getValidations());

        if("CATALOG".equalsIgnoreCase(mdrKey.getValidations().getValueDomainType())) {
            flatCatalogueMatrix.put(mdrKey.toString(), getFlattenedCatalogue(mdrKey.getCatalogue()));
        }
        readSlots(mdrKey);
    }

    /**
     * Loads the members of the given namespace
     *
     * @throws ExecutionException
     * @throws MdrConnectionException
     * @throws MdrInvalidResponseException
     * @throws MdrException
     */
    private void initMembers()
            throws ExecutionException, MdrConnectionException, MdrInvalidResponseException, MdrException, ValidatorException {
        initMembers(false);
    }

    /**
     * Loads the members of the given namespace
     *
     * @param preLoadDataelements
     *            should the whole dataelements be preloaded instead of loading validations and slots separately
     *
     * @throws ExecutionException
     * @throws MdrConnectionException
     * @throws MdrInvalidResponseException
     * @throws MdrException
     */
    private void initMembers(boolean preLoadDataelements)
            throws ExecutionException, MdrConnectionException, MdrInvalidResponseException, MdrException, ValidatorException {
        // get all mdrkeys from our namespaces
        List<Result> namespaceMembers = new ArrayList<>();
        for (String namespace : mdrConnection.getNamespaces()) {
            namespaceMembers.addAll(mdrConnection.getMdrClient().getNamespaceMembers(
                    Locale.getDefault().getLanguage(),
                    mdrConnection.getAccessTokenToken(),
                    mdrConnection.getAuthUserId(),
                    namespace));
        }

        for (Result member : namespaceMembers) {
            MdrKey mdrKey = new MdrKey(member.getId(), mdrConnection);
            initMember(mdrKey, preLoadDataelements);
        }
    }

    /**
     * Reads out the slots of a mdrkey and puts them into our slotMdrKeyMatrix
     *
     * @param mdrKey
     * @throws MdrConnectionException
     * @throws ExecutionException
     * @throws MdrInvalidResponseException
     * @throws MdrException
     */
    private void readSlots(MdrKey mdrKey) throws MdrConnectionException, ExecutionException, MdrInvalidResponseException, MdrException {
        for (Slot slot : mdrKey.getSlots()) {
            if (slotMdrKeyMatrix.containsKey(slot.getSlotName())) {
                slotMdrKeyMatrix.get(slot.getSlotName()).put(slot.getSlotValue(), mdrKey);
            } else {
                HashMap<String, MdrKey> temp = new HashMap<>();
                temp.put(slot.getSlotValue(), mdrKey);
                slotMdrKeyMatrix.put(slot.getSlotName(), temp);
            }
        }
    }

    /**
     * Validates the value against what is defined for its mdrkey
     * 
     * @param mdrKey
     * @param value
     * @return
     * @throws ValidatorException
     */
    public boolean validate(String mdrKey, Object value) throws ValidatorException {
        // reset catalog info
        lastValidatedWasCatalogueDesignation = false;

        if (!validationMatrix.containsKey(mdrKey)) {
            throw new ValidatorException("The key " + mdrKey + " does not exist in our namespace.");
        }

        String validatorType = validationMatrix.get(mdrKey).getValueDomainType();
        if("described".equalsIgnoreCase(validatorType)) {
            validatorType = validationMatrix.get(mdrKey).getValidationType();
            if (validatorType.equalsIgnoreCase("NONE") && validationMatrix.get(mdrKey).getDatatype().equalsIgnoreCase("STRING")) {
                validatorType = "STRING";
            }
        }
        validatorType = validatorType.toUpperCase();

        // no validator type, so this element is valid for anything
        if(validatorType == null || "NONE".equalsIgnoreCase(validatorType))
            return true;

        try {
            EnumValidatorType type = EnumValidatorType.valueOf(validatorType);

            Validator validator = ValidatorFactory.getValidator(type, validationMatrix.get(mdrKey).getUnitOfMeasure(), flatCatalogueMatrix.get(mdrKey));
            if (validator == null) {
                throw new ValidatorException(
                        "No validator found for the key " + mdrKey + " of type " + type.name() + ".");
            }

            MdrKey key = new MdrKey(mdrKey, mdrConnection);
            String caseSensitiveString = key.getSingleSlotValue(caseSensitive);
            if (caseSensitiveString != null && !Boolean.parseBoolean(caseSensitiveString.trim())) {
                validator.setCaseSensitive(false);
            }

            // validate!
            boolean validated = validator.validate(validationMatrix.get(mdrKey), value);

            // if a catalogue was validated, note if it was done by designation instead of code
            if(validated && validator instanceof CatalogueValidator) {
                lastValidatedWasCatalogueDesignation = ((CatalogueValidator) validator).getWasValidatedDesignation();
            }

            // just for debugging purposes
            if(!validated) {
                if (validator instanceof CatalogueValidator) {
                    logger.debug("CatalogueValidator says no to '"+value+"'");
                    logger.debug("Catalogue accepted codes are : "+ StringUtil.join(((CatalogueValidator) validator).getFlatCatalogue().getValidCodes(), ","));
                }
            }

            return validated;
        } catch (IllegalArgumentException e) {
            throw new ValidatorException(
                    "The validation type " + validatorType + " does not exist for mdr element "+mdrKey+".");
        } catch (MdrException | ExecutionException | MdrInvalidResponseException | MdrConnectionException e) {
            throw new ValidatorException(e);
        }
    }

    /**
     * Returns the list of error messages defined in MDR for a mdrKey
     * 
     * @param mdrKey
     * @return List<ErrorMessage>
     */
    public List<ErrorMessage> getErrorMessage(String mdrKey) {
        if (validationMatrix.get(mdrKey).getErrorMessages() != null
                && !validationMatrix.get(mdrKey).getErrorMessages().isEmpty())
            return validationMatrix.get(mdrKey).getErrorMessages();
        return null;
    }

    /**
     * Returns the MdrKey of a DTH BioInfName Slot entry
     * 
     * @param slotValue
     *            the DTH BioInfName Slot value
     * @return
     */
    public MdrKey getMdrKeyOfSlot(String slotName, String slotValue) {
        if (slotMdrKeyMatrix.get(slotName) != null)
            return slotMdrKeyMatrix.get(slotName).get(slotValue);
        else
            return null;
    }

    public static List<String> getAmbiguousCatalogues() {
        if(ambiguousCatalogues == null)
            ambiguousCatalogues = new ArrayList<>();
        return ambiguousCatalogues;
    }

    /**
     * Runs through a catalogue and flattens it out
     * flatCatalogues field is used as cache
     *
     * @param catalogue
     * @return the flattened catalogue
     */
    public FlatCatalogue getFlattenedCatalogue(Catalogue catalogue) {

        List<String> validCodes = new ArrayList<>();
        HashMap<String, Code> validDesignationToCodes = new HashMap<>();

        for(Code code: catalogue.getCodes()) {
            if(code.getIsValid()) {
                validCodes.add(code.getCode());
                for(Designation designation: code.getDesignations()) {
                    if(validDesignationToCodes.get(designation.getDesignation()) != null) {
                        // some other code already has this designation, check if the code is the same

                        if(!validDesignationToCodes.get(designation.getDesignation()).getCode().equals(code.getCode())) {
                            // this is a designation that belongs to another code already so we cannot really decide
                            // which code it will belong to for the transformation
                            getAmbiguousCatalogues().add(catalogue.getRoot().getIdentification().getUrn());
                            logger.warn("Ambiguous code for designation "+designation.getDesignation()+" in catalogue "+catalogue.getRoot().getIdentification().getUrn());
                        }
                    }
                    validDesignationToCodes.put(designation.getDesignation(), code);

                    if (validDesignationToCodes.get(designation.getDefinition()) != null) {
                        // some other code already has this definition, check if the code is the same

                        if (!validDesignationToCodes.get(designation.getDefinition()).getCode().equals(code.getCode())) {
                            // this is a designation that belongs to another code already so we cannot really decide
                            // which code it will belong to for the transformation
                            getAmbiguousCatalogues().add(catalogue.getRoot().getIdentification().getUrn());
                            logger.warn("Ambiguous code for definition " + designation
                                                                                    .getDefinition() + " in catalogue " + catalogue
                                                                                                                                   .getRoot()
                                                                                                                                   .getIdentification()
                                                                                                                                   .getUrn());
                        }
                    }
                    validDesignationToCodes.put(designation.getDefinition(), code);
                }
            }
        }

        return new FlatCatalogue(validCodes, validDesignationToCodes);
    }

    /**
     * Returns the flatCatalogueMatrix.
     * Used to find the code for a designation entry
     *
     * @return
     */
    public HashMap<String, FlatCatalogue> getFlatCatalogueMatrix() {
        return flatCatalogueMatrix;
    }

    /**
     * Information if the last validation was done by a catalogue designation instead of code
     * @return
     */
    public Boolean getLastValidatedWasCatalogueDesignation() {
        return lastValidatedWasCatalogueDesignation;
    }


}

