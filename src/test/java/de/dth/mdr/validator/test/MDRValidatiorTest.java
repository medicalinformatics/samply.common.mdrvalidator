package de.dth.mdr.validator.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;

import de.dth.mdr.validator.MDRValidator;
import de.dth.mdr.validator.MdrConnection;
import de.dth.mdr.validator.exception.MdrException;
import de.dth.mdr.validator.exception.ValidatorException;
import de.samply.common.config.Configuration;
import de.samply.common.config.ObjectFactory;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.ErrorMessage;
import de.samply.config.util.FileFinderUtil;
import de.samply.config.util.JAXBUtil;

public class MDRValidatiorTest {
    private static final String mdrURL = "https://mdr-dth.mitro.dkfz.de/v3/api/mdr";
    private static final String authUserId = "20";
    private static final String keyId = "2";
    private static final String authURL = "https://login.mitro.dkfz.de";
    private static final String privateKeyBase64 = "MIIJQQIBADANBgkqhkiG9w0BAQEFAASCCSswggknAgEAAoICAQCgjwoT/b16fxVjMbytIUYryC+YnZwrUdRCmxX8aTaeB0riup6NUscv2Bw8Z//G5i7mV7O6jpQEmhpbqlmyQDGXVGpJIovTs0U/WDrGhnshudvGUiRz4i/cYhnUd2JFKXNObZVs8PyoFluZt1INgcDnM0KM6bjflnzFGzyJq0DdpK4AswqeNlbV5Ge6Hx1e6iAUIM5dAorGHOGCCslzIYFOmgfD5KafrS3bhDg4QZp9roOkKAgmjSCrzePND012FQXyAhG/bkCCesm/G2Db4ih5h4ae2WTz88YNu5AAhWQd5yAjCk9s2GG0TeK4j5nAFrqql3Ms6RI1kImCr5erhjH9CrjLO+YK2tuHrynnyULcypmTKhhI22E0Ce6c4kKhQPfRzMe9H2b4djwDJAhftcveXu01FauB7QWqf+vwP30JlQ7V4jWA2o44q1IdzyzcWCEaJ7ebkXOy08ArocyNlquZVUppgdIVjV6sSyPOR4++pEBn9tKIwcN9qgp/dwoG+mwEWQtv74o221FINUgcc2OIHGQxkEJH4c3SsVK5HX5XwLYGDH4WJy+DibSvX5lxuM7Jv+I12oyckX6QbBdyWrkY/brq0SyUe57T93vyMc3aZDg+YXNuENkFiYqFhk56Rp/R+Bef3S/bcq3iShzjlgntPupIUATR2hdwEzFF5o0o8wIDAQABAoICADP6GmkeW5isS2AxWcMyYmUKCV5+p2/erbhiPFvaM7Q06Ck+ANX/VjxG2d00Tbk2CzjGa4iZX5Gd0aqbGy55WZSD32SNqnZ+MoyvI1fhcTsZz+wD64kYUCYoG/SMMWPyZ0GceCMsYJ6Jnl3E2utEdg5iqci0YQUA0qN8jbyDlBwsk6fmTimprbLQnkPCjt+LK5dYh3XUvNglwVs34r9CuXmYo6rm+on2pkHKK+kKU9kcBcG+s+THCyyTGovnyEvRwnr18IjoSDn1Rhp34oNMU44EMbB68BOJdzQ65ANHK4ICsISncc7qvbaBAER/OGbW9NcE8GQ5j4uHnvyPm6oLZ2Le38XyP7+Q2o71Y5MhSyB9Er77aret6EyDxez+4XMY6KrASnMXo5OY9Mcf+cc9VaHJo7YRPqYB8g5zK5J79PFwwamxDBa9EHBE3yE1RmIh2MKHuFb3je61n1TV9LxMjr8q+wdS0DlgD0DHtBv59IRfbIh2nbJnutLxvflOk6R4OXZn5XU4nttGSfC/zQANfR9Ogd2ms96kpCZb1+AoRhPmvyPGjcri8l1ZEA8eYkdt45E8TCqeui/eoiAe3jNabwuOcrNCPgJ3FCdHned9FdYQrLEyVUI6bzv/VRkKq0BUegkLFiqsZuFobfXDHjuwW3QqqozmYbRM6IOcMuWQM80BAoIBAQDpmo3Np8yQP4Paf3sehBI1KmBzHNLul3tDpq4G04HqaAES9YQs1GtsepBlr5LW8hU1LsupKDqFjx+87RnxQGH+9jYWL4r4Cgawp9yTy0sKbebqUTISPXVlpu5vd+RtwIXrQqmqb0E4efwgOLEXjIP16joo3W03OLuMU+oCSp0tc+uW1+l7hGxOgXlAjApQGPE/+0nDOlMOuI/eL5m2TSoviiNl1o0NA6KQuZ7zqI7+Kp/3bAdBTflQdepuco1XX/iX9qvJChvD7DfSxzb6j8sVd09Ggdy27h+zaMcvXv0q1IZ6V+Ue3xTqVoNcfmuWMHrZajnI0LkCwfsx0yv8D4JXAoIBAQCv87VMMg776T3xZsACzXFKS1YRN9oFHTEZdhDHLmw+rFglrhQ9l9IyMpTbrm7DeUP1jP3UIU8elNkU7QHYxAW7UdZmmJfyQseJuyu/ut2oHVTTqf/ktAprOe7xWLAYLoGI/X1C1Ja90Jl+12CW6JtbgI6u/kQEyJoxlmb4Qncr+wJY1uPrF7LvNdpsEvNQgH4CPFrDKqSZ5y35Yn7GGnFevt8bcBwDbFufPP4ZfJegQI3KbHIJ705kueTqvouFcWFL6taGkymxogbDufRsLHQB4btgXjTg2aM/VlktkYcmrG9Wwo5O5mzEZCJ8oS+eRFk5f2LiieFy+ueAkKmEP4TFAoIBAFpH0OXaI8Tbxyl6eyqgAClr5zqVuS9ce9b5Y+hfdaYR69hX6m0o6xtRgtzJrgZEKM4U98C4O2XCmpF5UL1cFINkxCJu3VZfCZbcSPMVbjrpnoSQPLmv5t8SVLPfsfh4n0j8ynWD6cDZ5AbP4iEnvRgdHb0NVlgxJMpm49le3L/kPPhfGW0TEIiGoUXA2Xt7KEGB7E/3CoJ18aP84XsC93rH+EYzVO/ip8Em+7dPPXQdJRWKOoOSjvf5mlB1FQrfnEOeoWVg8roVYL2lICpVyDbTlV+6cIKCJN5BcDlujIkw+Yfu0C3OKVcneD0W3p18vv2ngA2MHPRj46Ct7CHOHKECggEAYEEmUQShhdXW+RSIvf+9ljMYZlrzfhC2J4ObMhuHQ9HsdtCAaiF/784T+Qm3tl/EebQjt8RxD/3Fs9jkRb0WlZ7zdzTLCtDVb9dg24ZSdAF3wkMhpe+IM5iByq4Fx3TJkDN8Lu81d6mCDw2r3WJqeugdJkYa17MWB0KMGgviPcEXS5pjHNzeGlaPosfLK6LfTEHQNSxC157MW70yNRWKJ5arXss5x/WjOb2YFEFAgX5PEwm4aQ7tB9VaEcEjemDIJUvXB8/B41cL9E7qE7NN0ym148Ylj0wALkoChxcxpEp4rjHIwAj2P4m4BA35is9BMF8rrVpYZxozQvXghmRpMQKCAQA7ipiFZGZY7UhvrHgxQAoVkJG2aHmw0sXVF2MoetxAw612onpu9icp6O6zON37T60GXVnjIXYjxXoJnKBAnu4LFKykub800fxoPUb982V6KBicivtFvKmCeHyP1dSGi1w04jDDaEtae4Km8HMlgh4X6q6MllMpg07XHOtPROh3x+K5La3HewyolvODBRTtIbHaZDoh6M6HcSmA/ycqJigGcn3NkgZEYtN+bfVAGwPwH/G4TdsIwGuKYAEbFSjm4RM0ZLSfxn+Cz2CgDIMfAWocXNbFdgZDVs3LOyZUX84Hqe4+V4sWLgDnJgM7tiWp4gm2YVi0fnyXSeigUrkZeo7t";
    private static final String nameSpace = "dth";

    private static MdrConnection mdrConnection;

    @BeforeClass
    public static void init() throws JAXBException, SAXException, ParserConfigurationException, IOException, URISyntaxException {
        System.setProperty("jsse.enableSNIExtension", "false");

        String filePath = MDRValidatiorTest.class.getResource("/").getFile();
        
        File configFile = FileFinderUtil.findFile("proxy.xml", "dth.validator", filePath);
        assertNotNull(configFile);
        
        Configuration proxyConfiguration = JAXBUtil.unmarshall(configFile,
                JAXBContext.newInstance(ObjectFactory.class), Configuration.class);
        
        mdrConnection = new MdrConnection(mdrURL, authUserId, keyId, authURL, privateKeyBase64, nameSpace, proxyConfiguration, true);
    }
    
    @AfterClass
    public static void end() {
        mdrConnection.close();
    }
    
    @Test
    public void test() throws MdrConnectionException, MdrInvalidResponseException, ExecutionException, MdrException, ValidatorException {
        MDRValidator val = new MDRValidator(mdrConnection, "urn:dth:dataelementgroup:8:latest");
        assertNotNull(val);

        // element urn:dth:dataelement:151:2 is a float range 0.0<=x<=100.0 with % as unit of measure
        String mdrKey = "urn:dth:dataelement:151:2";

        boolean validated = val.validate(mdrKey, "1.06%");
        assertTrue(validated);
        if(!validated) {
            for (ErrorMessage error : val.getErrorMessage(mdrKey)) {
                System.out.println(error.getDesignation() + " : " + error.getDefinition());
            }
        }

        // also allows values without the unit of measure
        assertTrue(val.validate(mdrKey, "1.06"));

        // out of range, so should be false
        assertFalse(val.validate(mdrKey, "900.06%"));

//        // slot stuff test
//        val = new DTHValidator(mdrConnection, "urn:onkostar:dataelementgroup:12:latest");
//        mdrKey = "urn:onkostar:record:38:1";
//
//        System.out.println(val.getMdrKeyOfSlot("OnkostarName", "dTNMTpraefix"));
    }
}
